//geometry.go
package main

import (
	"fmt"
	"log"
	"test/Pktest"
	"test/Pktest/Rec"
)

var rectLen, rectWidth float64 = -6, 7

func initcode() {
	println("main package initialized")
	if rectLen < 0 {
		log.Fatal("length is less than zero")
	}
	if rectWidth < 0 {
		log.Fatal("width is less than zero")
	}
}
func main() {
	var rectLen, rectWidth float64 = 6, 7
	fmt.Println("Geometrical shape properties")

	/*Hàm Area của package rectangle được sử dụng
	 */
	fmt.Printf("area of rectangle %.2f\n", Pktest.Area(rectLen, rectWidth))
	/*Hàm Diagonal của package rectangle được sử dụng
	 */
	fmt.Printf("diagonal of the rectangle %.2f ", Pktest.Diagonal(rectLen, rectWidth))
	fmt.Printf(Rec.Sayhello())
	num := 10
	if num == 10 {
		fmt.Printf("yeah")
	} else {
		fmt.Printf("ahhhhhh")
	}
	i := 0
	for i <= 10 { //semicolons are ommitted and only condition is present
		fmt.Printf("%d ", i)
		i += 2

	}
	switch num := 75; { //num is not a constant
	case num < 200:
		fmt.Printf("%d is lesser than 200\n", num)
		fallthrough
	case num > 100:
		fmt.Printf("%d is greater than 100\n", num)
	case num < 500:
		fmt.Printf("%d is lesser than 500", num)
	}
}
